package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;
import ru.mtumanov.tm.enumerated.EntitySort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRq extends AbstractUserRq {

    @Nullable
    private EntitySort sort;

    public TaskListRq(@Nullable final String token, @Nullable final EntitySort sort) {
        super(token);
        this.sort = sort;
    }

}