package ru.mtumanov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.mtumanov.tm.component.Bootstrap;
import ru.mtumanov.tm.configuration.ClientConfiguration;

public final class Application {

    public static void main(@Nullable String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start(args);
    }

}
