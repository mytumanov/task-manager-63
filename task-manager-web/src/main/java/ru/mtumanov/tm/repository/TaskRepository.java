package ru.mtumanov.tm.repository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.mtumanov.tm.dto.model.TaskDTO;

public class TaskRepository {
    
    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    private static int i = 0;

    @NotNull
    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private TaskRepository() {}

    @NotNull
    private Map<String, TaskDTO> tasks = new LinkedHashMap<>();

    {
        save(new TaskDTO("One", "One desct"));
        save(new TaskDTO("Two", "Two desct"));
        save(new TaskDTO("Tree", "Tree desct"));
        save(new TaskDTO("Four", "Four desct"));
        save(new TaskDTO("Five", "Five desct"));
    }

    public void create() {
        save(new TaskDTO("New Task " + i++, String.valueOf(System.currentTimeMillis())));
    }

    public void save(@NotNull final TaskDTO task) {
        tasks.put(task.getId(), task);
    }

    @Nullable
    public Collection<TaskDTO> findAll() {
        return tasks.values();
    }

    @Nullable
    public TaskDTO findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }
}
