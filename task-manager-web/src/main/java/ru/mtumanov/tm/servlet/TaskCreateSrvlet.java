package ru.mtumanov.tm.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.mtumanov.tm.repository.TaskRepository;

@WebServlet("/task/create/*")
public class TaskCreateSrvlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TaskRepository.getInstance().create();
        resp.sendRedirect("/tasks");
    }
    
}
