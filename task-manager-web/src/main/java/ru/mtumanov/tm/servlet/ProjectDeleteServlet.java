package ru.mtumanov.tm.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.mtumanov.tm.repository.ProjectRepository;

@WebServlet("/project/delete/*")
public class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        ProjectRepository.getInstance().removeById(id);
        resp.sendRedirect("/projects");
    }
    
}
