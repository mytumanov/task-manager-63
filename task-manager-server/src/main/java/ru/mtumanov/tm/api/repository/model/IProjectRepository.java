package ru.mtumanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import ru.mtumanov.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    List<Project> findAllByUserId(@NotNull String userId, Sort sort);

}
